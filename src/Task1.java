public class Task1 {
    int[] numbers = new int[12];


    public Task1() {
        numbers[0] = 3;
        numbers[1] = 1;
        numbers[2] = 8;
        numbers[3] = 7;
        numbers[4] = 11;
        numbers[5] = 0;
        numbers[6] = 12;
        numbers[7] = 90;
        numbers[8] = 70;
        numbers[9] = 1;
        numbers[10] = 8;
        numbers[11] = 4;
    }

    public void sort() {
        for (int i = 0; i < numbers.length; i++) {
            for (int j = 0; j < numbers.length; j++) {
                if (j != numbers.length - 1)
                    if (numbers[j] > numbers[j + 1]) {
                        int k = numbers[j];
                        numbers[j] = numbers[j + 1];
                        numbers[j + 1] = k;
                    }
            }
        }
    }

    public void print(){
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);

        }
    }

    public static void main(String[] args) {
        Task1 task1 = new Task1();

        task1.sort();
        task1.print();

        Task2 task2 = new Task2();
    }
}

