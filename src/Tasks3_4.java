import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class Tasks3_4 {
    @Before
    public void startPrint(){
        System.out.println("Start printing user credentials");
    }

    @Test
    public void printUserCredentials(){
        User.ADMIN.print();
        User.GUEST.print();
        User.DIRECTOR.print();
    }

    @After
    public void finishPrint(){
        System.out.println("Finish printing user credentials");
    }
}

enum User{
    ADMIN("admin","123!W"),
    GUEST("Mary", "147iuy"),
    DIRECTOR("Ivan", "dfghjrty67");

    private String name;
    private String pass;

    User(String name, String pass) {
        this.name = name;
        this.pass = pass;
    }

    public void print(){
        System.out.println(name +" "+ pass);
    }
}