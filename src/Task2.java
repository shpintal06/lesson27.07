import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Task2 {
    private List<Integer> arrayList = new ArrayList();

    public Task2() {
        arrayList.add(1);
        arrayList.add(11);
        arrayList.add(8);
        arrayList.add(5);
        arrayList.add(9);
        arrayList.add(0);
    }

    public void sort() {
        arrayList.sort(Comparator.naturalOrder());
    }

    public void print() {
        for (Integer i : arrayList) {
            System.out.println(i);
        }
    }

    public static void main(String[] args) {
        Task2 task2 = new Task2();
        task2.sort();
        task2.print();
    }

}
